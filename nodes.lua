-- [Egbert14]
-- Nodes creation and manipulation

-- *****************************************************************************
-- ************************************************************************ Node
Node = {}
Node.mt = {} -- metatable

function Node.new( p, v, t )
   local node = { p=p, v=v, w=0, t=t}
   setmetatable( node, Node.mt ) -- make sur all share same metatable
   return node
end
function Node.tostring( node )
   local s = "NODE"
   s = s .. " Np=" .. tostring(node.p)
   s = s .. " Nv=" .. tostring(node.v)
   s = s .. " Nw=" .. tostring(node.w)
   s = s .. " Nt=" .. tostring(node.t)
   return s
end
Node.mt.__tostring = Node.tostring

function Node.tofile( node )
   local s = SM.tofile( node.p )
   s = s .. "\t" .. SM.tofile( node.v )
   s = s .. "\t" .. tostring( node.w )
   s = s .. "\t" .. tostring( node.t )
   return s
end
-- *****************************************************************************

-- *****************************************************************************
-- ************************************************************************ dist
function dist( Np, x )
   local norm = SM.dot( SM.diff(x, Np), SM.diff(x, Np) )
   --print( "  n=" .. tostring( norm ))
   local e = math.exp( - _Kd * norm )
   --print( "  e=" .. tostring( e ))
   return (2.0 * e) / ( 1.0 + e)
end
function weight( N )
   local e = math.exp( - _Kw * N.w )
   return (2.0) / ( 1.0 + e)
end
function phi( x, nodes )
   local sum = 0
   for _,node in pairs( nodes ) do
      sum = sum + weight( node ) * dist( node.p, x)
   end
   return sum
end
-- *****************************************************************************

function vel_attraction( vec, node )
   local attract = Gamma( SM.diff( vec, node.p ), node.v )
   local res = SM.add( node.v, attract )
   return M.proj( res )
end

function delta_mu( vec, nodes )
   local m_vel = M.zero()
   local m_att = M.zero()
   
   for _,node in pairs( nodes ) do
      --print( "VA pour", node )
      local vel = M.proj( node.v )
      -- if nodes.p == nil then
      --    print( "** ERROR d_mu with node=" .. tostring( node ))
      -- end
      local att = M.proj( Gamma( SM.diff( vec, node.p), node.v ))
      --print( "   va=" .. tostring( vel ) .. "/" .. tostring(att))

      local d = dist( node.p, vec )
      local w = weight(node)
      --print( "  w=" .. tostring(w) .. " d=" .. tostring( d ))
      
      vel = M.mul( w*d, vel )
      att = M.mul( w*d, att )
      --print( "   va=" .. tostring( vel ) .. "/" .. tostring(att))
      
      m_vel = M.add( m_vel, vel )
      m_att = M.add( m_att, att )
      --print( "-> sm=" .. tostring( m_vel ) .. "/" .. tostring(m_att))
   end
   local k = 1 / phi( vec, nodes )
   --print( "  k=" .. tostring( k ) )
   m_vel = M.mul( k, m_vel )
   m_att = M.mul( k, m_att )
   return m_vel, m_att, k
end

-- *****************************************************************************
-- ************************************************************************ plot
function plot_dmu( time, xrange, yrange, nodes, fout )
   for x = xrange.min, xrange.max, xrange.step do
      for y = yrange.min, yrange.max, yrange.step do
         local dmu_v, dmu_a, k = delta_mu( SM.new( x, y ), nodes )
         local s = tostring(time)
         s = s .. "\t" .. tostring( x )
         s = s .. "\t" .. tostring( y )
         s = s .. "\t" .. SM.tofile( dmu_v )
         s = s .. "\t" .. SM.tofile( dmu_a )
         s = s .. "\t" .. tostring( k )
         fout:write( s .. "\n" )
      end
   end
end
-- *****************************************************************************
