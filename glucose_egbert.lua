-- Reproduce XP with Glucose of [Egbert14a]
-- TEST condition : glucose level IS in the SensoriMotor State
-- CONTROL condition : glucose level IS NOT in the SensoriMotor State

-- Test arguments, essentially to switch CONTROL/TEST and to get
-- file extentions
local optarg = require "modules/lua-optarg/optarg"

opthelp = [[
Options:
 -h, --help              Help message
 -c, --control           Control condition (default is Test)
 -o, --outextension=OUT  Output extension for filename (def is TMP)
]]
opts, args = optarg.from_opthelp(opthelp)
if not opts or opts.help then
	print(("Usage: %s [-hc] [-o OUT] "):format(_G.arg[0]))
	print(opthelp)
	os.exit(opts and 1 or 0)
end

_control = opts["c"]~=nil      -- true if CONTROL
_outext = opts["o"] or "TMP"    -- extension is "TMP" by default"

-- *****************************************************************************
-- *********************************************************************** PARAM
_delay = 10.0
_Kd = 1000
_Kw = 0.0025
_Kt = 1.0
_dtsim = 0.1
_tsim_start = 10.0
_tsim_end = 40.0
_eval_time = 10.0
-- *****************************************************************************

-- Definition of SM,M, etc
if _control then
   require( "sm_3d2d" )
else
   require( "sm_2d3d" )
end
-- Definition of Metabolism
require( "metabolism" )
-- Definition of Nodes
require( "nodes" )

local random = require "random"
print( "random=" .. random.version )
local rnd = random.new()
rnd:seed( os.time() )

local sf = string.format
local pb = require "modules/progressbar"

-- *****************************************************************************
-- ************************************************ World, light sensors, motors
_world = { xmin=-4, xmax=4, ymin=-4, ymax=4 }
_pos = { x=0, y=0, alpha=0}
_radius = 0.25

function light_sensor( pos, beta )
   local sensor = { x = pos.x + _radius * math.cos( pos.alpha + beta ),
                    y = pos.y + _radius * math.sin( pos.alpha + beta ) }

   local bvec = { x = math.cos( pos.alpha + beta ),
                  y = math.sin( pos.alpha + beta ) }

   local dist = sensor.x * sensor.x + sensor.y * sensor.y

   -- positive part of dot product
   local val = bvec.x * (-sensor.x) + bvec.y * (-sensor.y)
   if val > 0 then
      val = val / math.sqrt( dist )
      val = val / ( 1 + dist )
      return val
   else
      return 0
   end
end

-- only sm.g should be affected in TEST
function SM.normalize( sm )
   if _control then return sm end
   local n = SM.new( sm.ml, sm.mr, sm.ll, sm.lr, (sm.g - 50)/(130.0 - 50.0) )
   return n
end
function unormalize( sm_n)
   if _control then return sm_n end
   local un = SM.new( sm_n.ml, sm_n.mr, sm_n.ll, sm_n.lr,
                      sm_n.g * (130.0 - 50.0) + 50.0 )
   return un
end

function step_motor( sm_n, dt, dmu_n )
   local sm_next_n = SM.add( sm_n, SM.mul( dt, dmu_n ))
   return sm_next_n
end
   
function step_position( pos, motor, dt )
   local xdot = math.cos( pos.alpha ) * ( motor.ml + motor.mr )
   local ydot = math.sin( pos.alpha ) * ( motor.ml + motor.mr )
   local alphadot = 2 * ( motor.mr - motor.ml )

   return { x = pos.x + xdot * dt,
            y = pos.y + ydot * dt,
            alpha = pos.alpha + alphadot * dt }
end

function pos_random()
   local pos = {
      x = rnd() * (_world.xmax - _world.xmin) + _world.xmin,
      y = rnd() * (_world.ymax - _world.ymin) + _world.ymin,
      alpha = rnd() * math.pi * 2.0
   }
   return pos
end
function pos_clamp( pos )
   if pos.x > _world.xmax then pos.x = _world.xmin + (pos.x - _world.xmax) end
   if pos.x < _world.xmin then pos.x = _world.xmax + (_world.xmin - pos.x) end
   
   if pos.y > _world.ymax then pos.y = _world.ymin + (pos.y - _world.ymax) end
   if pos.y < _world.ymin then pos.y = _world.ymax + (_world.ymin - pos.y) end

   pos.alpha = pos.alpha % (math.pi * 2.0)
   return pos
end

function strpos( pos )
   local s = "{ "
   s = s .. tostring( pos.x ) .. ", "
   s = s .. tostring( pos.y ) .. ", "
   s = s .. tostring( pos.alpha )
   s = s .. " }"
   return s
end
-- *****************************************************************************

-- *****************************************************************************
-- **************************************************** population / random walk
-- GLOBAL : _nodes
function random_walk( nb_ite )
   -- print( "__RANDOM WALK in SM" )
   -- random start
   local x = SM.rnd( rnd )
   -- print( "  start = " .. tostring(x) )

   for ite = 1, nb_ite-1 do
      -- random SM in [-0.05, 0.05]
      local r = SM.rnd( function() return rnd()*0.1-0.05 end)
      -- print( " r=" .. tostring(r) )
      for k,_ in pairs(x) do
         if x[k] + r[k] > 1 then
            r[k] = -r[k]
         elseif x[k] + r[k] < 0 then
            r[k] = -r[k]
         end
      end
      -- print( " r=" .. tostring(r))
      local node = Node.new( x, r, 0 )
      table.insert( _nodes, node )
      x = SM.add( x, r )
      -- print( string.format( "%3d", ite) .. ": x=" .. tostring(x) )
   end
end
-- GLOBAL : _nodes
function init_nodes( nb_walk, walk_size )
   for nw = 0, nb_walk-1 do
      random_walk( walk_size+1 )
   end
end

-- *****************************************************************************

-- *****************************************************************************
-- test random trajectory


_nodes = {}

_basename = ""
if _control then
   _basename = "control_"
else
   _basename = "glucose_"
end
_ftraj = assert( io.open( _basename .. "traj" .. _outext .. ".data", "w" ))
_fnodes = assert( io.open( _basename .. "nodes" .. _outext .. ".data", "w" ))

local start_t = os.clock()
init_nodes( 200, 50 )
print( sf("__INIT done in %.3f", os.clock() - start_t) .. "s")

-- starting position
_pos = pos_random()
_meta = Metabo.new( 90, 0, 0, _tsim_start )

_sm = SM.zero() -- TODO: RANDOM ??
if not _control then
   _sm.g = _meta.G
end
_sm_n = SM.normalize( _sm )
_old_sm_n = SM.add( _sm_n, SM.zero() )        -- crude COPY :o)
print( "START _sm=" .. tostring( _sm ))
print( "START _sm_n=" .. tostring( _sm_n ))

local active_nodes = {}

local progress = pb.ProgressBar:new()
progress:start( _tsim_end )
print( "__RUN for " .. tostring(_tsim_end) .." simulated time units" )
start_t = os.clock()
--_meta.cur_t = 10.0
--_meta.time = {10.0}
for tsim = _tsim_start, _tsim_end, _dtsim do
   --start_t = os.clock()
   --print( "__ t=" .. tsim )
   progress:update( tsim )
   -- active nodes
   active_nodes = {}
   local nb_active_nodes = 0
   for _,node in pairs( _nodes ) do
      if node.t < (tsim - _delay) then
         table.insert( active_nodes, node )
         nb_active_nodes = nb_active_nodes + 1
      end
   end
   --print( "nb_active=" .. tostring( nb_active_nodes ) )

   -- compute influence
   local motor_n = M.proj( _sm_n )               -- new speed
   if nb_active_nodes > 0 then
      local v, a, k = delta_mu( _sm_n, active_nodes )
      local dmu_n = M.add( v, a)
      motor_n = M.add( motor_n, M.mul( _dtsim, dmu_n ))
   end

   --print( "  motor=" .. tostring( motor_n ))
   -- should unnormalize, but not needed here.
   _sm.ml = motor_n.ml
   _sm.mr = motor_n.mr

   _pos = step_position( _pos, motor_n, _dtsim )   -- and new position
   --print( "  step_pos=" .. strpos( _pos ) )
   _pos = pos_clamp( _pos )
   --print( "  pos=" .. strpos( _pos ) )

   _sm.ll = light_sensor( _pos, math.pi / 3 )  -- and sensors
   _sm.lr = light_sensor( _pos, -math.pi / 3 )
   
   -- feeding ?
   local dist_center = (_pos.x * _pos.x) + (_pos.y * _pos.y)
   local dmg = 0
   if dist_center < 4 then  
      dmg = Metabo.step( _meta, 2, _dtsim )
   else
      dmg = Metabo.step( _meta, 0, _dtsim )
   end
   if not _control then
      _sm.g = _meta.G                            -- and glucose
   end
   
   _sm_n = SM.normalize( _sm )
   --print( "  _sm=" .. tostring( _sm ))
   --print( "  _sm_n=" .. tostring( _sm_n ))
   
   -- log traj
   local s = tostring( tsim )
   s = s .. "\t" .. tostring(_pos.x)
   s = s .. "\t" .. tostring(_pos.y)
   s = s .. "\t" .. tostring(_pos.alpha)
   -- s = s .. "\t" .. SM.tofile( _sm )
   s = s .. "\t" .. SM.tofile( _sm_n )
   s = s .. "\t" .. tostring( dmg )
   s = s .. "\t" .. tostring( nb_active_nodes )
   _ftraj:write( s .. "\n" )

   -- update nodes
   for _,n in pairs( _nodes ) do
      local r = 10.0 * dist( n.p, _sm_n )
      n.w = n.w + (-1.0 + r ) * _dtsim
      --local e = tostring(tsim) .. "\t" .. Node.tofile( n )
      --fnodes:write( e .. "\n" )
   end

   -- create node ?
   local density = phi( _sm_n, _nodes )
   if density < _Kt then --and tsim <  _t_endtrain then
      -- create Node, need to compute Nv
      local Nv = SM.mul( 1.0/_dtsim, SM.diff( _old_sm_n, _sm_n ))
      local node = Node.new( _sm_n, Nv, tsim )
      --print( "Create node", node, "phi=", density )
      table.insert( _nodes, node )

      -- log node
      local e = tostring(tsim) .. "\t" .. Node.tofile( node )
      _fnodes:write( e .. "\n" )
   end

   _old_sm_n = SM.add( _sm_n, SM.zero() )   -- crude copy

   --print( sf("  done in %.3f", os.clock() - start_t) .. "s")
end

_ftraj.close()
_fnodes.close()

-- runtime
print( sf("DONE in %.3f", os.clock() - start_t) .. "s")

-- dmg during the last _eval_time instants
--print( Metabo.dump( _meta ))
local dmg_start = Metabo.get_delayed( _meta, _tsim_end - _eval_time ).dmg
local dmg_end = Metabo.get_delayed( _meta, _tsim_end ).dmg
print( sf("Total dmg=%.3f (level=%.3f)", ( dmg_end - dmg_start ), dmg_end) )
-- count nodes
local nb = 0
for _ in pairs( _nodes ) do nb = nb+1 end
print( "Created " .. tostring(nb) .. " nodes" )

-- *****************************************************************************
-- -- test init of nodes using random walk
-- _nodes = {}
-- init_nodes( 200, 50 )

-- -- count nodes
-- local nb = 0
-- for _ in pairs( _nodes ) do nb = nb+1 end
-- print( "Created " .. tostring(nb) .. " nodes" )

-- -- print in file
-- _fnodes = assert( io.open( "random_walk.data", "w" ))
-- for _,n in pairs( _nodes ) do
--    local e = Node.tofile( n )
--    _fnodes:write( e .. "\n" )
-- end
-- _fnodes.close()




-- *****************************************************************************
-- **************************************************************** test sensors
-- -- test values of light sensors in various settings
-- -- write in file 'light_sensor.data'
-- _flight = assert( io.open( "light_sensor.data", "w" ))

-- -- move robot along vertical line at x = 1, y : -5 -> 5
-- for i=1,100 do
--    local pos = { x=1, y=-5 + i / 10, alpha = math.pi / 2 }
--    local sl = light_sensor( pos, math.pi / 3 )
--    local sr = light_sensor( pos, - math.pi / 3 )

--    local msg = tostring( pos.x ) .. "\t" .. tostring( pos.y ) .. "\t" .. tostring( pos.alpha )
--    msg = msg .. "\t" .. tostring( sl )
--    msg = msg .. "\t" .. tostring( sr )

--    _flight:write( msg .. "\n" )
-- end

-- _flight.close()
