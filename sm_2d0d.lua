-- Agent and Environment for [Egbert14] (fig 1,3,4)
-- Sensorimotor is {x,y}
-- Motor is {x,y}

-- *****************************************************************************
-- **************************************************************** Sensorimotor
SM = {} 
SM.mt = {}   -- metatable for SM

function SM.new( x, y )
   local sm = { x=x, y=y }
   setmetatable( sm, SM.mt ) -- make sur all share same metatable
   return sm
end
function SM.zero()
   return SM.new( 0, 0 )
end
function SM.add( sm1, sm2)
   local sum = SM.new( sm1.x + sm2.x, sm1.y + sm2.y )
   return sum
end
function SM.diff( sm1, sm2 )
   local diff = SM.new( sm2.x - sm1.x, sm2.y - sm1.y )
   return diff
end
function SM.dot( sm1, sm2 )
   return sm1.x * sm2.x + sm1.y * sm2.y
end
function SM.mul( scalar, sm )
   return SM.new( scalar * sm.x, scalar * sm.y )
end

function SM.tostring( vec )
   local s = "{"
   s = s .. " x=" .. tostring( vec.x )
   s = s .. ", y=" .. tostring( vec.y )
   return s .. "}"
end
SM.mt.__tostring = SM.tostring

function SM.tofile( vec )
   local s = tostring( vec.x ) .. "\t" .. tostring( vec.y )
   return s
end
-- *****************************************************************************

-- *****************************************************************************
-- *********************************************************************** Motor
-- Motor : is Sensorimotor in our case
M = SM       
function M.proj( sm )
   return M.new( sm.x, sm.y )
end
-- *****************************************************************************

-- *****************************************************************************
-- *********************************************************************** Gamma
function Gamma( a, Nv )
   local norm_Nv = math.sqrt( SM.dot( Nv, Nv ) )
   local normedNv = SM.new( Nv.x / norm_Nv, Nv.y / norm_Nv )
   -- print( "nNv = " .. tostring( normedNv ))
   local aNv = SM.dot( a, normedNv )
   local diff = SM.new( a.x - aNv * normedNv.x,
                        a.y - aNv * normedNv.y )
   return diff
end
-- *****************************************************************************

-- *****************************************************************************
-- ************************************************************************ test
local function test_gamma()
   u = SM.new( 1, 0 )
   v = SM.new( 0, 1 )
   uu = SM.new( 2, 0)
   di = SM.new( 1.2, 1.3 )


   print( "G(u, v)=" .. tostring(Gamma( u, v )))
   print( "G(u, v)=", Gamma( u, v ) )
   print( "G(uu, v)=" .. tostring( Gamma( uu, v )))
   print( "G(uu, u)=" .. tostring( Gamma( uu, u )))
   print( "G(di, u)=" .. tostring( Gamma( di, u )))
end
-- *****************************************************************************


