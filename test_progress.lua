-- ***************************************************************************
-- test ProgressBar module

local pb = require "modules/progressbar"
require "msleep"

print( "__START" )
prog25 = pb.ProgressBar:new()
prog50 = pb.ProgressBar:new()
sleep(1)
prog25:start( 25 )
prog50:start( 50 )
for i=0,25 do
   msleep(500)
   prog25:update( i )
   msleep(100)
   prog50:update( i )
end
print( "__END" )
