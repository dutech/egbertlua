-- Reproduce Fig4 of [Egbert14]

-- Definition of SM, M, etc
require( "sm_2d0d" )

-- Definition of Nodes
require( "nodes" )


-- *****************************************************************************
-- **************************************************************** forced_motor
function forced_motor( t )
   local ang = ( math.pi * 2.0 * t ) / 10.0
   return SM.new( 0.75 * math.cos( ang ), 0.75 * math.sin( ang ) )
end

function SM.normalize( motor )
   local n = SM.mul( 0.5, motor )
   n = SM.add( SM.new( 0.5, 0.5), n )
   return n
end
function SM.unormalize( motor_n )
   local un = SM.diff( SM.new( 0.5, 0.5), motor_n )
   un = SM.mul( 2.0, un )
   return un
end

function step_motor( sm_n, dt, dmu_n )
   local sm_next_n = SM.add( sm_n, SM.mul( dt, dmu_n ))
   return sm_next_n
end
-- *****************************************************************************
-- *****************************************************************************

function training( ftraj, fnodes, flog, fgrid )
   local sm = SM.zero()
   local sm_n = SM.normalize( sm )
   local active_nodes = {}
   
   for tsim = 0, 100.0, _dtsim do
      -- event
      if math.abs(tsim - _t_endtrain) < (_dtsim/2.0)  then
         print( "__PLOT DMU" )
         plot_dmu( tsim, {min=0, max=1.0, step=0.05}, {min=0, max=1.0, step=0.05},
            _nodes, fgrid )
      end
      -- motor event or step
      if math.abs(tsim - _t_endtest) < (_dtsim/2.0) then
         sm_n = SM.new( 0.5, 0.6 )
      elseif math.abs(tsim - 75.0) < (_dtsim/2.0) then
         sm_n = SM.new( 0.5, 0.5 )
      -- training ??
      elseif tsim < _t_endtrain then
         -- forced sm state
         sm = forced_motor( tsim )
         sm_n = SM.normalize( sm )
         
      else
         -- active nodes
         active_nodes = {}
         for _,node in pairs( _nodes ) do
            if node.t < (tsim - _delay) then
               table.insert( active_nodes, node )
            end
         end

         -- compute influence
         local v, a, k = delta_mu( sm_n, active_nodes )
         local dmu_n = SM.add( v, a)
         sm_n = SM.add( sm_n, SM.mul( _dtsim, dmu_n ))
         sm = SM.unormalize( sm_n )

         -- local sm_f = forced_motor( tsim )
         -- local sm_fn = SM.normalize( sm_f )
         -- local s = tostring( tsim )
         -- s = s .. "\n  v=" ..tostring( v )
         -- s = s .. "\n  a=" .. tostring( a )
         -- s = s .. "\n  k=" .. tostring( k )
         -- s = s .. "\n  dmu_n=" .. tostring( dmu_n )
         -- s = s .. "\n  sm_n= " .. tostring( sm_n )
         -- s = s .. "\n  sm_fn=" .. tostring( sm_fn )
         -- s = s .. "\n  sm=   " .. tostring( sm )
         -- s = s .. "\n  sm_f= " .. tostring( sm_f )
         -- flog:write( s .. "\n" )
      end

      -- log traj
      local s = tostring( tsim )
      s = s .. "\t" .. SM.tofile( sm )
      s = s .. "\t" .. SM.tofile( sm_n )
      ftraj:write( s .. "\n" )

      -- update nodes
      for _,n in pairs( _nodes ) do
         local r = 10.0 * dist( n.p, sm_n )
         n.w = n.w + (-1.0 + r ) * _dtsim
         local e = tostring(tsim) .. "\t" .. Node.tofile( n )
         fnodes:write( e .. "\n" )
      end
      
      -- create node ?
      local density = phi( sm_n, _nodes )
      if density < _Kt then --and tsim <  _t_endtrain then
         -- create Node, need to compute Nv
         local Nv = SM.zero()
         local next_sm_n = SM.zero()
       
         -- training ??
         if tsim < _t_endtrain then
            local next_sm = forced_motor( tsim+_dtsim )
            next_sm_n = SM.normalize( next_sm )
     
            --elseif tsim < _t_endtest then
         else
            local v, a = delta_mu( sm_n, active_nodes )
            local dmu_n = SM.add( v, a)
            next_sm_n = SM.add( sm_n, SM.mul( _dtsim, dmu_n ))
         end
            
         local Nv = SM.mul( 1.0/_dtsim, SM.diff( sm_n, next_sm_n ))
         local node = Node.new( sm_n, Nv, tsim )
         print( "Create node", node, "phi=", density )
         table.insert( _nodes, node )

         -- log node
         local e = tostring(tsim) .. "\t" .. Node.tofile( node )
         fnodes:write( e .. "\n" )
      end
   end
end


-- *****************************************************************************
_Kd = 1000
_Kw = 0.0025
_Kt = 1.0
_delay = 10.0
_dtsim = 0.1
_t_endtrain = 20.0
_t_endtest = 30.0

_nodes = {}

_ftraj = assert( io.open( "fig4_traj.data", "w" ))
_fnodes = assert( io.open( "fig4_nodes.data", "w" ))
_fgrid = assert( io.open( "fig4_grid.data", "w" ))
--_flog = assert( io.open( "fig4_log.data", "w" ))

training( _ftraj, _fnodes, io.stdout, _fgrid )

_ftraj.close()
_fnodes.close()
_fgrid.close()
--_flog.close()

-- count nodes
local nb = 0
for _ in pairs( _nodes ) do nb = nb+1 end
print( "Created " .. tostring(nb) .. " nodes" )



