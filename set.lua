-- Defines Sets and operations on them
-- sets are implemented as table of key elements which are true
-- This makes some operations easier to optimize

Set = {}
Set.mt = {}    -- metatable for sets


function Set.new( t )
   local set = {}
   setmetatable( set, Set.mt ) -- make sur all share same metatable
   for _, elem in pairs(t) do set[elem] = true end
   return set
end

function Set.clone( set )
   local res = Set.new{}
   for k in pairs( set ) do res[k] = true end
   return res
end

function Set.union( a, b )
   local res = Set.new{}
   for k in pairs( a ) do res[k] = true end
   for k in pairs( b ) do res[k] = true end
   return res
end

function Set.intersection( a, b )
   local res = Set.new{}
   for k in pairs( a ) do
      res[k] = b[k] -- only if in b too
   end
   return res
end

Set.mt.__le = function (a,b)    -- set containment
   for k in pairs(a) do
      if not b[k] then return false end
   end
   return true
end
Set.mt.__lt = function (a,b)
   return a <= b and not (b <= a)
end
Set.mt.__eq = function (a,b)    -- set equality
   return a <= b and b <= a
end

function Set.remove( set, elem )
   set[elem] = nil
   return set
end
function Set.substract( a, b )
   for k in pairs(b) do a[k] = nil end
   return a
end
      

function Set.tostring( set )
   local s = "{"
   local sep = ""
   for e in pairs( set ) do
      s = s .. sep .. e
      sep = ","
   end
   return s .. "}"
end
Set.mt.__tostring = Set.tostring

-- *****************************************************************************
-- ************************************************************************ test
local s1 = Set.new{1,2,3}
local s2 = Set.new{4,5}
local s3 = Set.new{2,4}
local s4 = Set.new{}

print( "s1=",s1 )
print( "s2=",s2 )
print( "s3=",s3 )
print( "s4=",s4 )

print( "s1 U s2 =", Set.union( s1, s2 ) )
print( "s1 U s3 =", Set.union( s1, s3 ) )
print( "s1 U s4 =", Set.union( s1, s4 ) )

assert( Set.union( s1, s2 ) == Set.new{ 1,2,3,4,5 }, "Error s1 U s2" )
assert( Set.union( s1, s3 ) == Set.new{ 1, 2, 3, 4 }, "Error s1 U s3" )
assert( Set.union( s1, s4 ) == s1, "Error s1 U s4" )

c1 = Set.clone( s1 )
assert( c1 == s1, "Error Set.clone( s1 )" )

print( "s1 \\ 2 = ", Set.remove( s1, 2 ))
print( "s1 = ", s1 )
print( "s1 \\ 2 = ", Set.remove( s1, 2 ))
print( "s1 \\ 4 = ", Set.remove( s1, 4 ))
assert( Set.substract( s2, s4 ) == s2, "Error s2 \\ s4" )

print( "c1=",c1 )
-- *****************************************************************************
