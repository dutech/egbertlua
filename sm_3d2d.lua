-- Agent and Environment for [Egbert14],exp3 and [Egbert14a], with Glucose
-- Control situation
-- Sensorimotor is {ll,lr}
-- Motor is {ml,mr,md} where md stands for motor_dummy

-- *****************************************************************************
-- **************************************************************** Sensorimotor
SM = {} 
SM.mt = {}   -- metatable for SM

function SM.new( ml, mr, md, ll, lr)
   local sm = { ml=ml, mr=mr, md=md, ll=ll, lr=lr }
   setmetatable( sm, SM.mt ) -- make sur all share same metatable
   return sm
end

function SM.zero()
   return SM.new( 0, 0, 0, 0, 0 )
end
function SM.rnd( func )
   return SM.new( func(), func(), func(), func(), func() )
end

function SM.add( sm1, sm2)
   local sum = SM.zero()
   for k,_ in pairs( sm1 ) do
      sum[k] = sm1[k] + sm2[k]
   end
   return sum
end
function SM.diff( sm1, sm2 )
   local diff = SM.zero()
   for k,_ in pairs( sm1 ) do
      diff[k] = sm2[k] - sm1[k]
   end
   return diff
end
function SM.dot( sm1, sm2 )
   local dot = 0
   for k,_ in pairs( sm1 ) do
      dot = dot + sm1[k] * sm2[k]
   end
   return dot
end
function SM.mul( scalar, sm )
   local mul = SM.zero()
   for k,_ in pairs( sm ) do
      mul[k] = scalar * sm[k]
   end
   return mul
end

function SM.tostring( vec )
   local s = "{"
   s = s .. " ml=" .. string.format( "%.3f", vec.ml )
   s = s .. " mr=" .. string.format( "%.3f", vec.mr )
   s = s .. " md=" .. string.format( "%.3f", vec.md )
   s = s .. ", ll=" .. string.format( "%.3f", vec.ll )
   s = s .. " lr=" .. string.format( "%.3f", vec.lr )
   return s .. "}"
end
SM.mt.__tostring = SM.tostring

function SM.tofile( vec )
   local s = tostring( vec.ml ) .. "\t" .. tostring( vec.mr ) .. "\t" .. tostring( vec.md )
   s = s .. "\t" .. tostring( vec.ll ) .. "\t" .. tostring( vec.lr )
   return s
end
-- *****************************************************************************

-- *****************************************************************************
-- *********************************************************************** Motor
-- Motor : is SM.ml, SM.mr, SM.md
M = {} 
M.mt = {}   -- metatable for M

function M.new( ml,mr,md )
   local m = { ml=ml, mr=mr, md=md }
   setmetatable( m, M.mt ) -- make sur all share same metatable
   return m
end
function M.proj( sm )
   return M.new( sm.ml, sm.mr, sm.md )
end
function M.zero()
   return M.proj( SM.zero() )
end


function M.add( m1, m2 )
   local sum = M.zero()
   for k,_ in pairs( m1 ) do
      sum[k] = m1[k] + m2[k]
   end
   return sum
end
function M.diff( m1, m2 )
   local diff = M.zero()
   for k,_ in pairs( m1 ) do
      diff[k] = m2[k] - m1[k]
   end
   return diff
end
function M.mul( scalar, m )
   local mul = M.zero()
   for k,_ in pairs( m ) do
      mul[k] = scalar * m[k]
   end
   return mul
end

function M.tostring( m )
   local s = "{"
   s = s .. " ml=" .. string.format( "%.3f", m.ml )
   s = s .. " mr=" .. string.format( "%.3f", m.mr )
   s = s .. " md=" .. string.format( "%.3f", m.md )
   return s .. "}"
end
M.mt.__tostring = M.tostring

function M.tofile( m )
   local s = tostring( m.ml ) .. "\t" .. tostring( m.mr ) .. "\t" .. tostring( m.md )
   return s
end
-- *****************************************************************************

-- *****************************************************************************
-- *********************************************************************** Gamma
function Gamma( a, Nv )
   local norm_Nv = math.sqrt( SM.dot( Nv, Nv ) )
   local normedNv = SM.new( Nv.ml / norm_Nv, Nv.mr / norm_Nv,
                            Nv.md / norm_Nv,
                            Nv.ll / norm_Nv, Nv.lr / norm_Nv
   )
   -- print( "nNv = " .. tostring( normedNv ))
   local aNv = SM.dot( a, normedNv )
   local diff = SM.new( a.ml - aNv * normedNv.ml, a.mr - aNv * normedNv.mr,
                        a.md - aNv * normedNv.md,
                        a.ll - aNv * normedNv.ll, a.lr - aNv * normedNv.lr
   )
   return diff
end
-- *****************************************************************************


-- ************************************************************************ test
local function test()
   local m1 = SM.new( 1, 2, 3, 4, 5 )
   local m2 = SM.new( 1, 1, 1, 1, 1 )
   local m3 = SM.add( m1, m2 )
   print( "__ADD  = " .. tostring(SM.add(m1, m2)) )
   print( "__DIFF = " .. tostring(SM.diff(m1, m2)) )
   print( "__DOT  = " .. tostring(SM.dot(m1, m2)) )
   print( "__MUL  = " .. tostring(SM.mul(2, m1)) )       
   print( "__FILE = " .. SM.tofile( m1 ) )
end
