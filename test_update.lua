-- Check that can updte nodes IN tables
n1 = {x=2}
n2 = {x=4}
nodes = {n1,n2}

function pn()
   print( "__Nodes" )
   for _,n in pairs( nodes ) do
      print( "  x=", n.x )
   end
end

pn()
for _,n in pairs( nodes ) do
   n.x = n.x + 3
end
pn()

