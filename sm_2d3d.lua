-- Agent and Environment for [Egbert14],exp3 and [Egbert14a], with Glucose
-- Sensorimotor is {ll,lr,g}
-- Motor is {ml,mr}

-- *****************************************************************************
-- **************************************************************** Sensorimotor
SM = {} 
SM.mt = {}   -- metatable for SM

function SM.new( ml, mr, ll, lr, g )
   local sm = { ml=ml, mr=mr, ll=ll, lr=lr, g=g }
   setmetatable( sm, SM.mt ) -- make sur all share same metatable
   return sm
end
function SM.zero()
   return SM.new( 0, 0, 0, 0, 0 )
end
function SM.rnd( func )
   return SM.new( func(), func(), func(), func(), func() )
end

function SM.add( sm1, sm2)
   local sum = SM.new( sm1.ml + sm2.ml, sm1.mr + sm2.mr,
                       sm1.ll + sm2.ll, sm1.lr + sm2.lr, sm1.g + sm2.g )
   return sum
end
function SM.diff( sm1, sm2 )
   local diff = SM.new( sm2.ml - sm1.ml, sm2.mr - sm1.mr,
                        sm2.ll - sm1.ll, sm2.lr - sm1.lr, sm2.g - sm1.g )
   return diff
end
function SM.dot( sm1, sm2 )
   return sm1.ml * sm2.ml + sm1.mr * sm2.mr +
      sm1.ll * sm2.ll + sm1.lr * sm2.lr + sm1.g * sm2.g
end
function SM.mul( scalar, sm )
   return SM.new( scalar * sm.ml, scalar * sm.mr,
                  scalar * sm.ll, scalar * sm.lr, scalar * sm.g )
end

function SM.tostring( vec )
   local s = "{"
   s = s .. " ml=" .. string.format( "%.3f", vec.ml )
   s = s .. " mr=" .. string.format( "%.3f", vec.mr )
   s = s .. ", ll=" .. string.format( "%.3f", vec.ll )
   s = s .. " lr=" .. string.format( "%.3f", vec.lr )
   s = s .. " g=" .. string.format( "%.3f", vec.g )
   return s .. "}"
end
SM.mt.__tostring = SM.tostring

function SM.tofile( vec )
   local s = tostring( vec.ml ) .. "\t" .. tostring( vec.mr )
   s = s .. "\t" .. tostring( vec.ll ) .. "\t" .. tostring( vec.lr )
   s = s .. "\t" .. tostring( vec.g )   
   return s
end
-- *****************************************************************************

-- *****************************************************************************
-- *********************************************************************** Motor
-- Motor : is SM.ml, SM.mr
M = {} 
M.mt = {}   -- metatable for M

function M.new( ml,mr )
   local m = { ml=ml, mr=mr }
   setmetatable( m, M.mt ) -- make sur all share same metatable
   return m
end
function M.proj( sm )
   return M.new( sm.ml, sm.mr )
end
function M.zero()
   return M.proj( SM.zero() )
end

function M.add( m1, m2 )
   return M.new( m1.ml + m2.ml, m1.mr + m2.mr )
end
function M.diff( m1, m2 )
   local diff = M.new( m2.ml - m1.ml, m2.mr - m1.mr )
   return diff
end
function M.mul( scalar, m )
   return M.new( scalar * m.ml, scalar * m.mr )
end

function M.tostring( m )
   local s = "{"
   s = s .. " ml=" .. string.format( "%.3f", m.ml )
   s = s .. " mr=" .. string.format( "%.3f", m.mr )
   return s .. "}"
end
M.mt.__tostring = M.tostring

function M.tofile( m )
   local s = tostring( m.ml ) .. "\t" .. tostring( m.mr )
   return s
end
-- *****************************************************************************

-- *****************************************************************************
-- *********************************************************************** Gamma
function Gamma( a, Nv )
   local norm_Nv = math.sqrt( SM.dot( Nv, Nv ) )
   local normedNv = SM.new( Nv.ml / norm_Nv, Nv.mr / norm_Nv,
                            Nv.ll / norm_Nv, Nv.lr / norm_Nv,
                            Nv.g / norm_Nv )
   -- print( "nNv = " .. tostring( normedNv ))
   local aNv = SM.dot( a, normedNv )
   local diff = SM.new( a.ml - aNv * normedNv.ml, a.mr - aNv * normedNv.mr,
                        a.ll - aNv * normedNv.ll, a.lr - aNv * normedNv.lr,
                        a.g - aNv * normedNv.g )
   return diff
end
-- *****************************************************************************

-- *****************************************************************************
-- ************************************************************************ test
local function test_gamma()
   u = SM.new( 1, 0, 1, 2, 3 )
   v = SM.new( 0, 1, 0.5, -1, 0 )
   uu = SM.new( 2, 0, 2, 2, 1)
   di = SM.new( 1.2, 1.3, 2.1, 2.2, 2.3 )


   print( "G(u, v)=" .. tostring(Gamma( u, v )))
   print( "G(u, v)=", Gamma( u, v ) )
   print( "G(uu, v)=" .. tostring( Gamma( uu, v )))
   print( "G(uu, u)=" .. tostring( Gamma( uu, u )))
   print( "G(di, u)=" .. tostring( Gamma( di, u )))
end
-- *****************************************************************************


