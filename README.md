# Reformulation of [Egbert2014]
## RUN

`lua glucose_egbert.lua [-hc] [-o OUT]`


## Required and provided modules
### msleep : provides 'sleep' and 'msleep'
- compile using `gcc -shared -fPIC -o msleep.so -I/usr/include/lua5.3 -llua5.3 msleep.c`
- create link at root level : `ln -s modules/msleep/msleep.so`

### progressbar : provides ProgressBar
- usage : `local pb = require( "modules/progressbar" )`

### lrandom-100 : provides random
- set the right `LUA_TOPDIR` in Makefile
- compile using `make`
- create link at root level : `ln -s modules/lrandom-100/random.so`

### lua-optarg : provides optional command line arguments
- clone from `https://github.com/ncopa/lua-optarg.git`
- in dir ../LuaLibs
- then the link should work :o)
- usage : `local optarg = require "modules/lua-optarg/optarg"`

