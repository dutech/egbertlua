-- module ProgressBar
local module = {}

-- ***************************************************************************
-- *************************************************************** ProgressBar
-- ProgressBar that display XX.X% |#####.................|
--
-- Create pb = ProgressBar:new()
-- Start  pb:start( 100 ) --maxval
-- Update pb:update( val )
-- (see test_progress)

module.ProgressBar = {
   val=0,
   maxval=100,
   maxwidth = 80
}
-- ******************************************************************** format
-- bar in the form '|###.......|'
function module.ProgressBar:format() -- self is implicit argument
   local frac = self.val / self.maxval
   
   local msg = ""
   msg = msg .. string.format( "%2.1f%%", frac * 100.0 )
   msg = msg .. " "
   local leftspace = self.maxwidth - string.len(msg) - 2  --left and right marker
   local nbchar = math.floor( leftspace * frac )
   msg = msg .. "|" .. string.rep( "#", nbchar )
   nbchar = self.maxwidth - string.len(msg) - 1           -- right marker
   msg = msg .. string.rep( ".", nbchar ) .. "|"

   return msg
end
-- ********************************************************************* start
function module.ProgressBar:start( maxval )
   self.maxval = maxval or self.maxval
   self.val = 0

   -- look for terminal width
   local handle = io.popen( "tput cols" )
   local termwidth = handle:read()
   handle:close()
   self.maxwidth = math.min( 80, tonumber(termwidth) )
end
-- ******************************************************************** update
function module.ProgressBar:update( val )
   self.val = math.min( val, self.maxval )
   local bar = self:format()

   local ending = "\r"
   if self.val == self.maxval then
      ending = "\n"
   end
   io.stderr:write( bar .. ending )
end
-- *********************************************************************** new
-- The idea to have a class is to use itsel (ProgressBar) as metatable and
-- and for any function/field missing, using __index
function module.ProgressBar:new( o )
   o = o or {}                 -- create object if user does not provide one
   setmetatable( o, self )
   self.__index = self         -- where to look for missing functions
   return o
end
-- *********************************************************** ProgressBar END
-- ***************************************************************************

return module


