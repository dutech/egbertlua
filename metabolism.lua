-- Internal metabolism with Glucose (G), Insuline (I) and Glucagon (U)
-- (E) is the nutrition/entry level
--
-- Keep an history of G,I,U,dmg

Metabo = {}
-- metatable for SM
Metabo.mt = {}
-- default values
Metabo.param = {c=1.0,
                f_u=20,   f_i=0.1,
                b_u=60,   b_i=120,
                c_u=0.25, c_i=0.25,
                d_u=1.0,  d_i=1.0,
                tau=5.0 }
-- time[] holds time at which new histories are created
-- WARN : time[] must grow monotonically with index
function Metabo.new( g,i,u, start_time )
   local cur_t = start_time or 0
   local meta = { G=g, I=i, U=u, cur_t=cur_t,
                  time={cur_t}, hist={{G=g,I=i,U=u,dmg=0}} }
   meta.p = {}
   for k,v in pairs(Metabo.param) do
      meta.p[k] = v
   end
   setmetatable( meta, Metabo.mt ) -- make sur all share same
   return meta
end

function Metabo.get_delayed( meta, time )
   --print( "__GET_DELAYED time=" .. tostring(t) )
   for i,t in ipairs(meta.time) do
      --print( "  check t["..i.."]="..t )
      if t >= time then
         if i > 1 then
            return meta.hist[i-1]
         else
            return meta.hist[1]
         end
      end
   end
   print( "__METABOLISM : delayed not found with time=" .. tostring(time) .. " and t=" .. tostring(t) .. " i=" .. tostring(i) )
end
function Metabo.heavyside( val )
   if val < 0 then
      return 0
   else
      return 1
   end
end
-- Update cur_t and G,U,V
-- append to time and hist
function Metabo.step( meta, E, dt )
   --print( "__STEP" )
   --print( "  delay" .. meta.p.tau.. " -> " ..tostring(meta.cur_t-meta.p.tau))
   local v_delay = Metabo.get_delayed( meta, meta.cur_t - meta.p.tau )
   local dG = E + meta.p.f_u * meta.U
      - meta.p.f_i * meta.G * meta.I
      - meta.p.c

   local dI = Metabo.heavyside( v_delay.G - meta.p.b_i ) * meta.p.c_i
      - meta.p.d_i * meta.I

   local dU = Metabo.heavyside( meta.p.b_u - v_delay.G ) * meta.p.c_u
      - meta.p.d_u * meta.U


   meta.G = meta.G + dG * dt
   meta.I = meta.I + dI * dt
   meta.U = meta.U + dU * dt

   -- dmg when outside of borne
   local dmg = meta.hist[#(meta.hist)].dmg
   --print( "  dmg_before=" .. tostring(dmg) )
   if meta.G > meta.p.b_i or meta.G < meta.p.b_u then
      dmg = dmg + dt
   end
   
   meta.cur_t = meta.cur_t + dt
   table.insert( meta.time, meta.cur_t )
   table.insert( meta.hist, {G=meta.G, I=meta.I, U=meta.U, dmg=dmg} )

   return dmg
end

local function sf( val )
   return string.format( "%.3f", val)
end
function Metabo.GUVtostring( GUV )
   local s = ""
   s = s .. "G=" .. sf( GUV.G )
   s = s .. " I=" .. sf( GUV.I )
   s = s .. " U=" .. sf( GUV.U )
   return s
end
function Metabo.tostring( meta )
   local s = "t= " .. sf( meta.cur_t ) .. " : "
   s = s .. Metabo.GUVtostring( {G=meta.G, I=meta.I, U=meta.U} )
   return s
end
Metabo.mt.__tostring = Metabo.tostring
function Metabo.dump( meta )
   local s = Metabo.tostring( meta )
   s = s .. "\n"
   for i,t in ipairs( meta.time ) do
      print( "dumping with i=" .. i .. " t=" .. tostring(meta.time[i]) )
      print( "        hist  =" .. tostring(meta.hist[i]) )
      s = s .. "  [" .. sf(t) .. "]:{" .. Metabo.GUVtostring( meta.hist[i] ) .. " dmg=" .. sf(meta.hist[i].dmg) .. "}\n"
   end
   return s
end

-- *****************************************************************************
-- ************************************************************* Only Metabolism
-- -- Generate file "fig1_metabo.data" to rescience Fig1 of [Egbert14a]
-- -- t G I U for E=2 and t G U I for E=0
-- _delta_t = 0.1
-- _fmeta = assert( io.open( "fig1_metabo.data", "w" ))

-- me = Metabo.new( 90, 0, 0)
-- m0 = Metabo.new( 90, 0, 0)
-- for i=1,850 do
--    Metabo.step( me, 2, _delta_t )
--    Metabo.step( m0, 0, _delta_t )

--    local s = tostring(me.cur_t)
--    s = s .. "\t" .. tostring(me.G)
--    s = s .. "\t" .. tostring(me.I)
--    s = s .. "\t" .. tostring(me.U)
--    s = s .. "\t" .. tostring(m0.cur_t)
--    s = s .. "\t" .. tostring(m0.G)
--    s = s .. "\t" .. tostring(m0.I)
--    s = s .. "\t" .. tostring(m0.U)
--    _fmeta:write( s .. "\n" )
-- end
-- _fmeta.close()



-- *****************************************************************************
-- ************************************************************************ test
-- m = Metabo.new( 62, 0, 0)
-- for k,v in pairs( m ) do print( k, v) end
-- print( "m = ".. tostring(m) )
-- print( "m.time = " .. tostring(m.time) )
-- print( "  time[1] = " .. tostring(m.time[1]) )
-- print( "m.hist = " .. tostring(m.hist) )
-- print( "  hist[1] = " .. tostring(m.hist[1]) )
-- print( "DUMP(m)= " .. Metabo.dump( m ) )

-- for i=1,30 do
--    print( "__STEP" )
--    Metabo.step( m, 0, 0.1 )
--    print( "DUMP(m)= " .. Metabo.dump( m ) )
-- end

       
