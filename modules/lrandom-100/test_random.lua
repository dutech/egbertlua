-- Test random generation

local random = require "random"
print( "random=" .. random.version )
local rnd = random.new()
rnd:seed( os.time() )

for ite=1, 100 do
   print( ite, rnd(), rnd:value(), rnd:value(1,10), rnd:value(1, 2.5) )
end
