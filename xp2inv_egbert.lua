-- Reproduce XP2 (fig 5) of [Egbert14]
-- BUT node creation BEFORE node update

-- Definition of SM, M, etc
require( "sm_1d1d" )

-- Definition of Nodes
require( "nodes" )


-- *****************************************************************************
-- **************************************************************** forced_motor
function forced_motor( t )
   local ang = ( t /2.0 )
   --return 0.5 * math.cos( ang )
   return math.cos( ang )
end

function light_sensor( x )
   return 1.0 / ( 1.0 + x * x )
end


function SM.normalize( sm )
   local n = SM.new( sm.m * 0.25 + 0.5, sm.s )
   return n
end
function unormalize( motor_composante )
   local un = motor_composante - 0.5 
   un = 4.0 * un
   return un
end

function step_motor( sm_n, dt, dmu_n )
   local sm_next_n = SM.add( sm_n, SM.mul( dt, dmu_n ))
   return sm_next_n
end
-- *****************************************************************************
-- *****************************************************************************

function training( ftraj, fnodes, flog, fgrid )
   local x = -2.5
   local sm = SM.zero()
   local sm_n = SM.normalize( sm )
   local active_nodes = {}
   if _dmu_events == nil then _dmu_events = {} end
   
   for tsim = 0, 75.1, _dtsim do
      -- event
      if math.abs(tsim - _t_endtest) < (_dtsim/2.0)  then
         -- relocate
         x = -2.5
         -- option : reset also sm and sm_n
         sm = SM.new( 0, light_sensor( x ))
         sm_n = SM.normalize( sm )
                      
      end
      -- check if dmu for the grid must be plotted
      for _,ev in pairs( _dmu_events ) do
         if math.abs(tsim - ev) < (_dtsim/2.0) then
            print( "__PLOT DMU at " .. tostring(ev) )
            -- active nodes
            active_nodes = {}
            for _,node in pairs( _nodes ) do
               if node.t < (tsim - _delay) then
                  table.insert( active_nodes, node )
               end
            end
            plot_dmu( tsim,
                      {min=0, max=1.0, step=0.05},
                      {min=0, max=1.0, step=0.05},
                      active_nodes,
                      fgrid )
         end
      end
      
      -- step
      if tsim < _t_endtrain then
         -- forced motor state
         sm.m = forced_motor( tsim )
         x = x + sm.m * _dtsim
         sm.s = light_sensor( x )
         sm_n = SM.normalize( sm )
         
         
      else
         -- active nodes
         active_nodes = {}
         for _,node in pairs( _nodes ) do
            if node.t < (tsim - _delay) then
               table.insert( active_nodes, node )
            end
         end

         -- compute influence
         local v, a, k = delta_mu( sm_n, active_nodes )
         local dmu_n = M.add( v, a)
         sm_n.m = sm_n.m + _dtsim * dmu_n.m  -- give new speed
         sm.m = unormalize( sm_n.m )
         x = x + sm.m * _dtsim               -- and new position
         sm.s = light_sensor( x )
         sm_n = SM.normalize( sm )


         -- local sm_f = SM.zero()
         -- sm_f.m = forced_motor( tsim )
         -- local x_f = x + sm_f.m * _dtsim
         -- sm_f.s = light_sensor( x_f )
         -- local sm_fn = SM.normalize( sm_f )
         -- local s = tostring( tsim )
         -- s = s .. "\n  v=" ..tostring( v )
         -- s = s .. "\n  a=" .. tostring( a )
         -- s = s .. "\n  k=" .. tostring( k )
         -- s = s .. "\n  dmu_n=" .. tostring( dmu_n )
         -- s = s .. "\n  x=" .. tostring( x ) .. "\txf=" .. tostring(x_f)
         -- s = s .. "\n  sm_n= " .. tostring( sm_n )
         -- s = s .. "\n  sm_fn=" .. tostring( sm_fn )
         -- s = s .. "\n  sm=   " .. tostring( sm )
         -- s = s .. "\n  sm_f= " .. tostring( sm_f )
         -- flog:write( s .. "\n" )
      end

      -- log traj
      local s = tostring( tsim )
      s = s .. "\t" .. tostring(x)
      s = s .. "\t" .. SM.tofile( sm )
      s = s .. "\t" .. SM.tofile( sm_n )
      ftraj:write( s .. "\n" )

      -- create node ?
      local density = phi( sm_n, _nodes )
      if density < _Kt then --and tsim <  _t_endtrain then
         -- create Node, need to compute Nv
         local Nv = SM.zero()
         local next_sm_n = SM.zero()
       
         -- training ??
         if tsim < _t_endtrain then
            local next_sm = SM.zero()
            next_sm.m = forced_motor( tsim+_dtsim )
            local next_x = x + next_sm.m * _dtsim
            next_sm.s = light_sensor( next_x )
            next_sm_n = SM.normalize( next_sm )
     
            --elseif tsim < _t_endtest then
         else
            local next_sm = SM.zero()
            local v, a = delta_mu( sm_n, active_nodes )
            local dmu_n = M.add( v, a)
            next_sm_n.m = sm_n.m + _dtsim * dmu_n.m  -- give new speed
            next_sm.m = unormalize( next_sm_n.m )
            local next_x = x + next_sm.m * _dtsim    -- and new position
            next_sm.s = light_sensor( next_x )
            next_sm_n = SM.normalize( next_sm )
         end
            
         local Nv = SM.mul( 1.0/_dtsim, SM.diff( sm_n, next_sm_n ))
         local node = Node.new( sm_n, Nv, tsim )
         print( "Create node", node, "phi=", density )
         table.insert( _nodes, node )

         -- log node
         local e = tostring(tsim) .. "\t" .. Node.tofile( node )
         fnodes:write( e .. "\n" )
      end

      -- active nodes
      active_nodes = {}
      for _,node in pairs( _nodes ) do
         if node.t < (tsim - _delay) then
            table.insert( active_nodes, node )
         end
      end
   end
end


-- *****************************************************************************
_Kd = 1000
_Kw = 0.0025
_Kt = 1.0
_delay = 10.0
_dtsim = 0.1
_t_endtrain = 20.0
_t_endtest = 35.0
_dmu_events = { 15.0, 35.0, 75.0 }

_nodes = {}

_ftraj = assert( io.open( "fig5_traj.data", "w" ))
_fnodes = assert( io.open( "fig5_nodes.data", "w" ))
_fgrid = assert( io.open( "fig5_grid.data", "w" ))
--_flog = assert( io.open( "fig4_log.data", "w" ))

training( _ftraj, _fnodes, io.stdout, _fgrid )

_ftraj.close()
_fnodes.close()
_fgrid.close()
--_flog.close()

-- count nodes
local nb = 0
for _ in pairs( _nodes ) do nb = nb+1 end
print( "Created " .. tostring(nb) .. " nodes" )



