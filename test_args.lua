-- Test command line arguments : module lua-optarg

local optarg = require "modules/lua-optarg/optarg"

opthelp = [[
Options:
 -h, --help              Help message
 -c, --control           Control condition
 -o, --outextension=OUT  Output extension for filename
]]

opts, args = optarg.from_opthelp(opthelp)

if not opts or opts.help then
	print(("Usage: %s [-hc] [-o OUT] "):format(_G.arg[0]))
	print(opthelp)
	os.exit(opts and 1 or 0)
end

for k,v in pairs(opts) do
	print ("__OPT:", k,"=",v)
end

print( "__CONTROL = ", opts["c"]~=nil )

print( "__ARG= " .. tostring( arg ))
for k,v in pairs( arg ) do
   print( k .. " = " .. v )
end

