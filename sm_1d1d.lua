-- Agent and Environment for [Egbert14],exp2
-- Sensorimotor is {m,s}
-- Motor is {m}

-- *****************************************************************************
-- **************************************************************** Sensorimotor
SM = {} 
SM.mt = {}   -- metatable for SM

function SM.new( m, s )
   local sm = { m=m, s=s }
   setmetatable( sm, SM.mt ) -- make sur all share same metatable
   return sm
end
function SM.zero()
   return SM.new( 0, 0 )
end
function SM.add( sm1, sm2)
   local sum = SM.new( sm1.m + sm2.m, sm1.s + sm2.s )
   return sum
end
function SM.diff( sm1, sm2 )
   local diff = SM.new( sm2.m - sm1.m, sm2.s - sm1.s )
   return diff
end
function SM.dot( sm1, sm2 )
   return sm1.m * sm2.m + sm1.s * sm2.s
end
function SM.mul( scalar, sm )
   return SM.new( scalar * sm.m, scalar * sm.s )
end

function SM.tostring( vec )
   local s = "{"
   s = s .. " x=" .. tostring( vec.m )
   s = s .. ", l=" .. tostring( vec.s )
   return s .. "}"
end
SM.mt.__tostring = SM.tostring

function SM.tofile( vec )
   local s = tostring( vec.m ) .. "\t" .. tostring( vec.s )
   return s
end
-- *****************************************************************************

-- *****************************************************************************
-- *********************************************************************** Motor
-- Motor : is SM.m
M = {} 
M.mt = {}   -- metatable for M

function M.new( m )
   local m = { m=m }
   setmetatable( m, M.mt ) -- make sur all share same metatable
   return m
end
function M.proj( sm )
   return M.new( sm.m )
end
function M.zero()
   return M.proj( SM.zero() )
end

function M.add( m1, m2 )
   return M.new( m1.m + m2.m )
end
function M.diff( m1, m2 )
   local diff = M.new( m2.m - m1.m )
   return diff
end
function M.mul( scalar, m )
   return M.new( scalar * m.m )
end

function M.tostring( m )
   local s = "{"
   s = s .. " m=" .. tostring( m.m )
   return s .. "}"
end
M.mt.__tostring = M.tostring

function M.tofile( m )
   local s = tostring( m.m )
   return s
end
-- *****************************************************************************

-- *****************************************************************************
-- *********************************************************************** Gamma
function Gamma( a, Nv )
   local norm_Nv = math.sqrt( SM.dot( Nv, Nv ) )
   local normedNv = SM.new( Nv.m / norm_Nv, Nv.s / norm_Nv )
   -- print( "nNv = " .. tostring( normedNv ))
   local aNv = SM.dot( a, normedNv )
   local diff = SM.new( a.m - aNv * normedNv.m,
                        a.s - aNv * normedNv.s )
   return diff
end
-- *****************************************************************************

-- *****************************************************************************
-- ************************************************************************ test
local function test_gamma()
   u = SM.new( 1, 0 )
   v = SM.new( 0, 1 )
   uu = SM.new( 2, 0)
   di = SM.new( 1.2, 1.3 )


   print( "G(u, v)=" .. tostring(Gamma( u, v )))
   print( "G(u, v)=", Gamma( u, v ) )
   print( "G(uu, v)=" .. tostring( Gamma( uu, v )))
   print( "G(uu, u)=" .. tostring( Gamma( uu, u )))
   print( "G(di, u)=" .. tostring( Gamma( di, u )))
end
-- *****************************************************************************


