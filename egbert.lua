-- [Egbert14]
-- Node creation

-- Definition of SM, M, etc
require( "sm_2d0d" )


-- *****************************************************************************
-- ************************************************************************ Node
Node = {}
Node.mt = {} -- metatable

function Node.new( p, v, t )
   local node = { p=p, v=v, w=0, t=t}
   setmetatable( node, Node.mt ) -- make sur all share same metatable
   return node
end
function Node.tostring( node )
   local s = "NODE"
   s = s .. " Np=" .. tostring(node.p)
   s = s .. " Nv=" .. tostring(node.v)
   s = s .. " Nw=" .. tostring(node.w)
   s = s .. " Nt=" .. tostring(node.t)
   return s
end
Node.mt.__tostring = Node.tostring
-- *****************************************************************************

-- *****************************************************************************
-- ************************************************************************ dist
function dist( Np, x )
   local norm = SM.dot( SM.diff(x, Np), SM.diff(x, Np) )
   --print( "  n=" .. tostring( norm ))
   local e = math.exp( - _Kd * norm )
   --print( "  e=" .. tostring( e ))
   return (2.0 * e) / ( 1.0 + e)
end
function weight( N )
   local e = math.exp( - _Kw * N.w )
   return (2.0) / ( 1.0 + e)
end
function phi( x, nodes )
   local sum = 0
   for _,node in pairs( nodes ) do
      sum = sum + weight( node ) * dist( node.p, x)
   end
   return sum
end

function vel_attraction( vec, node )
   local attract = Gamma( SM.diff( vec, node.p ), node.v )
   local res = SM.add( node.v, attract )
   return M.proj( res )
end

function delta_mu( vec, nodes )
   local sm_res = SM.zero()
   for _,node in pairs( nodes ) do
      --print( "VA pour", node )
      local va = vel_attraction( vec, node )
      --print( "   va=" .. tostring( va ))
      va = SM.mul( dist( node.p, vec ), va )
      --print( "   va=" .. tostring( va ) .. " (dist=" .. tostring( dist( node.p, vec )) .. ")" )
      va = SM.mul( weight(node), va )
      --print( "   va=" .. tostring( va ) .. " (w=" .. tostring( weight(node)) .. ")" )
      sm_res = SM.add( sm_res, va )
      --print( "-> sm=" .. tostring( sm_res ))
   end
   local k = 1 / phi( vec, nodes )
   --print( "  k=" .. tostring( k ) )
   sm_res = SM.mul( k, sm_res )
   return sm_res
end

-- *****************************************************************************
-- ************************************************************************ test
_Kd = 1000
_Kw = 0.0025

local function test_nodes()
local n1 = Node.new( {x=0}, {vx=0.2}, 0 )
local n2 = Node.new( {x=0.1}, {vx=0.14}, 1.2 )
_nodes = { n1, n2 }

print( "n1 : " .. Node.tostring( n1 ))
print( "n2 : " .. Node.tostring( n2 ))

-- Phi
print( "w(n1) = " .. tostring( weight( n1 )))
print( "w(n2) = " .. tostring( weight( n2 )))

print( "d(n1.p, 0.5) = " .. tostring( dist( n1.p, 0.5 )) )
print( "d(n2.p, 0.5) = " .. tostring( dist( n2.p, 0.5 )) )
print( "phi(0.5) = " .. phi( 0.5 ))

print( "d(n1.p, 0.1) = " .. tostring( dist( n1.p, 0.1 )) )
print( "d(n2.p, 0.1) = " .. tostring( dist( n2.p, 0.1 )) )
print( "phi(0.1) = " .. phi( 0.1 ))
end

local function test_attraction()
   local n1 = Node.new( SM.new( 0.5, 0.5), SM.new( 0, 0.1 ), 0 )
   print( "n1 = " .. Node.tostring( n1 ) )

   local vec = SM.new( 0.45, 0.45 )
   print( "vec = " .. tostring( vec ))
   
   print( "va( vec, n1 ) = " .. tostring( vel_attraction( vec, n1 ) ))
   v,a = delta_mu( vec, {n1} )
   print( "d_mu( vec, {n1} ) = " .. tostring( v ) .. " / " .. tostring(a) )
   
end

local function distance_data()
   -- open file
   local fdata = assert( io.open( "distance.data", "w" ))
   
   for x = 0, 0.15, 0.005 do
      local norm = x*x
      --print( "  n=" .. tostring( norm ))
      local e = math.exp( - _Kd * norm )
      --print( "  e=" .. tostring( e ))
      local d = (2.0 * e) / ( 1.0 + e)

      local s = tostring( x )
      s = s .. "\t" .. tostring( d )
      fdata:write( s .. "\n" )
   end
   fdata.close()
end

local function weight_data()
   -- open file
   local fdata = assert( io.open( "weight.data", "w" ))
   
   for w = -2000, 2000, 10 do
      local e = math.exp( - _Kw * w )
      local res = (2.0) / ( 1.0 + e)
      local s = tostring( w )
      s = s .. "\t" .. tostring( res )
       fdata:write( s .. "\n" )
   end
   fdata.close()
end

local function weight_data()
   -- open file
   local fdata = assert( io.open( "weight.data", "w" ))
   
   for w = -2000, 2000, 10 do
      local e = math.exp( - _Kw * w )
      local res = (2.0) / ( 1.0 + e)
      
      local s = tostring( w )
      s = s .. "\t" .. tostring( res )
      fdata:write( s .. "\n" )
   end
   fdata.close()
end
   
local function attraction_grid()
   local n1 = Node.new( SM.new( 0.5, 0.5), SM.new( 0, 0.1 ), 0 )
   print( "n1 = " .. Node.tostring( n1 ) )

   -- open file
   local fdata = assert( io.open( "one_node.data", "w" ))
   
   for x= 0.35, 0.65, 0.01 do
      for y= 0.35, 0.65, 0.01 do
         local dmu_v, dmu_a = delta_mu( SM.new( x, y ), {n1} )
         local dmu = SM.add( v, a )
         local s = tostring( x )
         s = s .. "\t" .. tostring( y )
         s = s .. "\t" .. tostring( dmu.x )
         s = s .. "\t" .. tostring( dmu.y )
         fdata:write( s .. "\n" )
      end
   end

   fdata.close()
   
end
local function four_node()
   local n1 = Node.new( SM.new( 0.5, 0.55), SM.new( 0.1, 0.0 ), 0 )
   local n2 = Node.new( SM.new( 0.55, 0.5), SM.new( 0.0, -0.1 ), 0 )
   local n3 = Node.new( SM.new( 0.5, 0.45), SM.new( -0.1, 0.0 ), 0 )
   local n4 = Node.new( SM.new( 0.45, 0.5), SM.new( 0.0, 0.1 ), 0 )

   local vec = SM.new( 0.6, 0.6 )
   print( n1)
   print( "va( vec, n1 ) = " .. tostring( vel_attraction( vec, n1 ) ))
   print( n2 )
   print( "va( vec, n2 ) = " .. tostring( vel_attraction( vec, n2 ) ))
   print( n3)
   print( "va( vec, n3 ) = " .. tostring( vel_attraction( vec, n3 ) ))
   print( n4 )
   print( "va( vec, n4 ) = " .. tostring( vel_attraction( vec, n4 ) ))
   local v,a = delta_mu( vec, {n1,n2,n3,n4})
   local dmu = SM.add( v, a )
   print( "dm=", dm )

   print( n1)
   print( "va( vec, n1 ) = " .. tostring( vel_attraction( vec, n1 ) ))
   n2.w = -500
   print( n2 )
   print( "va( vec, n2 ) = " .. tostring( vel_attraction( vec, n2 ) ))
   print( n3)
   print( "va( vec, n3 ) = " .. tostring( vel_attraction( vec, n3 ) ))
   print( n4 )
   print( "va( vec, n4 ) = " .. tostring( vel_attraction( vec, n4 ) ))
   v,a = delta_mu( vec, {n1,n2,n3,n4})
   dmu = SM.add( v, a )
   print( "dm=", dm )
   
end
   
local function fig3()
   local n1 = Node.new( SM.new( 0.5, 0.55), SM.new( 1, 0.0 ), 0 )
   local n2 = Node.new( SM.new( 0.55, 0.5), SM.new( 0.0, -1 ), 0 )
   local n3 = Node.new( SM.new( 0.5, 0.45), SM.new( -1, 0.0 ), 0 )
   local n4 = Node.new( SM.new( 0.45, 0.5), SM.new( 0.0, 1 ), 0 )

   -- open file
   local fdata = assert( io.open( "fig3.data", "w" ))

   for _,w in pairs({-500, -100, 0, 50, 100}) do
      n2.w = w
      for x= 0.35, 0.65, 0.01 do
         for y= 0.35, 0.65, 0.01 do
            local v, a = delta_mu( SM.new( x, y ), {n1,n2,n3,n4} )
            local dmu = SM.add( v, a )
            local s = tostring( x )
            s = s .. "\t" .. tostring( y )
            s = s .. "\t" .. tostring( dmu.x )
            s = s .. "\t" .. tostring( dmu.y )
            s = s .. "\t" .. tostring( w )
            fdata:write( s .. "\n" )
         end
      end
   end

   fdata.close()
end

--distance_data()
--weight_data()
--four_node()
fig3()


  

